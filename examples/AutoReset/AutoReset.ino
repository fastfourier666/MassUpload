/*
 * AutoReset example
 * 
 * Allows remote firmware load to be triggered from the network by resetting the ESP32
 * 
 * Configure the ssid, pass and url for your situation, then
 * run the relevant script in a terminal in the sketch folder
 * (watch.sh or watch.ps1, depending on your OS)
 * 
 */

#define MASSUPLOAD_OUTPUT_SERIAL Serial       // If you want serial output from the lib, define a Serial object here
#include "MassUpload.h"
#include <WifiUdp.h>

const char* ssid = "--YOUR--SSID--";
const char* pass = "--YOUR--PASSWORD--";
const char* url  = "http://192.168.42.86:10101/AutoReset.ino.esp32.bin";

WiFiUDP udp;

void setup() {
  Serial.begin(115200);
  delay(500);                                 // wait for serial monitor to wake up
  MassUpload.checkFirmware(ssid, pass, url);  // Check and download new firmware

  // Listen on UDP 52999
  if (WiFi.status() == WL_CONNECTED) {
    udp.begin(52999);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  udpCheck();
}

// Check for reset command on UDP
void udpCheck() {
  if (WiFi.status() != WL_CONNECTED) return;
  int packetSize = udp.parsePacket();
  char buf[128];
  if (packetSize) {
    int len = udp.read(buf, 128);
    if (len > 0) {
      buf[len] = 0;
      if (strstr(buf, "RESET")) ESP.restart();
    }
  }
}
