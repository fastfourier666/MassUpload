# resetter for ESP32 MassUploader - Mac OS & Linux
# requires socat and fswatch - install these with brew (Mac OS) or your Linux package manager
# in a terminal, cd to your sketch folder and do ./watch.sh
# you may have to chmod +x ./watch.sh first.
# 
# for some reason the "Updated" event doesn't fire on Linux so we're looking for AttributeModified
# as well. This means "touch"ing the .bin file will also trigger the ESP reset.

#!/bin/sh

fswatch -0 -x --event Updated --event AttributeModified ./*.bin | while read -d "" event; do
    echo $event
    echo "RESET\n" | socat - UDP-DATAGRAM:255.255.255.255:52999,broadcast
done