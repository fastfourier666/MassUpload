# Powershell resetter for ESP32 MassUploader
# with thanks to 
# https://powershell.one/tricks/filesystem/filesystemwatcher
# and
# https://gist.github.com/PeteGoo/21a5ab7636786670e47c

function Send-UdpDatagram
{
  Param ([string] $EndPoint, 
  [int] $Port, 
  [string] $Message)

  $IP = [System.Net.Dns]::GetHostAddresses($EndPoint) 
  $Address = [System.Net.IPAddress]::Parse($IP) 
  $EndPoints = New-Object System.Net.IPEndPoint($Address, $Port) 
  $Socket = New-Object System.Net.Sockets.UDPClient 
  $EncodedText = [Text.Encoding]::ASCII.GetBytes($Message) 
  $SendMessage = $Socket.Send($EncodedText, $EncodedText.Length, $EndPoints) 
  $Socket.Close() 
} 

# specify the path to the folder you want to monitor:
$Path = $pwd;

# specify which files you want to monitor
$FileFilter = '*.bin'  

# specify whether you want to monitor subfolders as well:
$IncludeSubfolders = $false

# specify the file or folder properties you want to monitor:
$AttributeFilter = [IO.NotifyFilters]::FileName, [IO.NotifyFilters]::LastWrite 

# specify the type of changes you want to monitor:
$ChangeTypes = [System.IO.WatcherChangeTypes]::Changed

# specify the maximum time (in milliseconds) you want to wait for changes:
$Timeout = 1000

# define a function that gets called for every change:
function Invoke-SomeAction
{
  param
  (
    [Parameter(Mandatory)]
    [System.IO.WaitForChangedResult]
    $ChangeInformation
  )
  
  Write-Warning 'Change detected:'
  $ChangeInformation | Out-String | Write-Host -ForegroundColor DarkYellow
  Write-Warning 'Sending UDP...'
  Send-UdpDatagram -EndPoint "255.255.255.255" -Port 52999 -Message "RESET"

}

# use a try...finally construct to release the
# filesystemwatcher once the loop is aborted
# by pressing CTRL+C

try
{
  Write-Warning "FileSystemWatcher is monitoring $Path for changes in $FileFilter files"
  
  # create a filesystemwatcher object
  $watcher = New-Object -TypeName IO.FileSystemWatcher -ArgumentList $Path, $FileFilter -Property @{
    IncludeSubdirectories = $IncludeSubfolders
    NotifyFilter = $AttributeFilter
  }

  # start monitoring manually in a loop:
  do
  {
    # wait for changes for the specified timeout
    # IMPORTANT: while the watcher is active, PowerShell cannot be stopped
    # so it is recommended to use a timeout of 1000ms and repeat the
    # monitoring in a loop. This way, you have the chance to abort the
    # script every second.
    $result = $watcher.WaitForChanged($ChangeTypes, $Timeout)
    # if there was a timeout, continue monitoring:
    if ($result.TimedOut) { continue }
    
    Invoke-SomeAction -Change $result
    # the loop runs forever until you hit CTRL+C    
  } while ($true)
}
finally
{
  # release the watcher and free its memory:
  $watcher.Dispose()
  Write-Warning 'FileSystemWatcher removed.'
}

