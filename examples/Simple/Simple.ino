/*
 * Simple example
 * 
 * Basic example of how to call the library's checkFirmware function.
 * 
 * Configure the ssid, pass, url, ip and mask for your situation.
 * Uncomment one of the checkFirmware calls depending on whether you want DHCP or static IP
 * 
 */

#define MASSUPLOAD_OUTPUT_SERIAL Serial       // If you want serial output from the lib, define a Serial object here
#include "MassUpload.h"

const char* ssid = "--YOUR--SSID--";
const char* pass = "--YOUR--PASSWORD--";
const char* url  = "http://192.168.42.86:10101/SimpleExample.ino.esp32.bin";
IPAddress ip = IPAddress(192,168,42,188);
IPAddress mask = IPAddress(255,255,255,0);

void setup() {
  Serial.begin(115200);
  delay(500);                                 // wait for serial monitor to wake up

  // UNCOMMENT ONE OF THE TWO LINES BELOW
//  MassUpload.checkFirmware(ssid, pass, url);            // Conenct to wifi with DHCP
//  MassUpload.checkFirmware(ssid, pass, ip, mask, url);  // Connect to wifi with a static IP
}

void loop() {
}
