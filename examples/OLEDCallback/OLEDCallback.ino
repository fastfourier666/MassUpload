/*
 * OLEDCallback example
 * 
 * Prints status updates on OLED while downloading new firmware.
 * 
 * Configure the ssid, pass and url for your situation, then
 * connect a 128X64 I2C OLED to ESP32 GPIO 21 (SDA) and 22 (SCL)
 * Requires u8g2 (install with library manager)
 */
 
#define MASSUPLOAD_OUTPUT_SERIAL Serial       // If you want serial output from the lib, define a Serial object here
#include "MassUpload.h"
#include <U8g2lib.h>
#include <Wire.h>

const char* ssid = "--YOUR--SSID--";
const char* pass = "--YOUR--PASSWORD--";
const char* url  = "http://192.168.42.86:10101/OLEDCallback.ino.esp32.bin";

U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);

void setup() {
  u8g2.begin();
  u8g2.setFont(u8g2_font_ncenB08_tr);         // choose a suitable font
  Serial.begin(115200);
  delay(500);                                 // wait for serial monitor to wake up
  MassUpload.setCallback (progress);          // set up callback for updates
  MassUpload.checkFirmware(ssid, pass, url);  // Check and download new firmware
}

void loop() {
}

// if the MassUpload library has something to say, print it on the OLED
void progress (const char* message) {
  u8g2.clearBuffer();
  u8g2.drawStr(0,10,message);
  u8g2.sendBuffer();
}
