# esp32\_mass\_upload

A library + setup details for uploading new Arduino sketches to many ESP32s at the same time over wifi. Could be used for ESP8266 as well possibly?

## a note about using this
Please don't try and use this for production anything. There is no https and no authentication. Once you have something working and the firmware is done, you should remove this library completely from your sketch!

## how to install
Download and extract zip. Rename the folder to "MassUpload", removing the "-main" or "-master" or whatever, and copy it into the Arduino libraries folder: `~/Documents/Arduino/libraries` or `C:\Users\yourname\My Documents\Arduino\libraries`

## a note about the examples
Before running any examples, please "Save As..." them to your normal sketchbook folder.
We will use the "export compiled binary" feature of the IDE, which writes a .bin file to the sketch folder. By default, the IDE prevents writing to anywhere contained in "Arduino/libraries".

Note: Unless you do [this](https://arduino.github.io/arduino-cli/0.19/library-specification/#development-flag-file), but it's much easier to just save the example somewhere else!

## in the sketch

### enable OTA
Choose a parition scheme from the Arduino IDE Tools menu which has some OTA space - i.e. none of the "No OTA" options!

### Enable serial output
e.g. `#define MASSUPLOAD_OUTPUT_SERIAL Serial`  
The library will call `.printf()` on the object defined above to display info while it's doing stuff. Comment out this line to keep it quiet.

### include the library
`#include <MassUpload.h>`  
important! This has to be done _after_ the define above if you want serial output!

### open the serial port
If you chose to use an output, open it first! e.g.`Serial.begin(115200);`
### define and register a callback function
You can define a callback function to receive status updates while the firmware is being updated. This could be useful to display the firmware download status on an LCD if you have no serial port connected.  
The callback will display a lot less info than the serial output defined above. You'll get short messages regarding wifi errors or HTTP errors, and while the file is being downloaded there will be a call for every % complete.

Example:
```
void firmwareProgress (const char* msg) {
	lcd.clear();
	lcd.print (msg);
}
```

Register the callback with the library:

`MassUpload.setCallback (firmwareProgress);`


### Do the check for updated firmware
Typically you will do this fairly early in `setup()`. If you want to load new firmware, there's not much point in running the old firmware for too long now is there. There are a few different versions:  

`MassUpload.checkFirmware (ssid, pass, ip, mask, url)` if you want the library to connect to wifi and use a static IP.  
`MassUpload.checkFirmware (ssid, pass, url)` if you want the library to connect to wifi and use DHCP.  
`MassUpload.checkFirmware (url)` if you want to handle wifi connection yourself outside of the library.

The function will return false on any errors (wifi or HTTP). It will return true only if the web server's firmware date matches the one currently on the ESP.

Note: ***this function blocks!*** until it either returns or reboots the ESP after a successful firmware download. If you need to do other stuff in the sketch while it's working, run it in a task (completely untested).

Note2: This function does not disconnect from the wifi network before it returns! If you want to use ESP stuff in your sketch that conflicts with wifi (e.g. ADC2) or you want to use wifi in another mode (AP etc), just call `MassUpload.stopWifi();` and then do your thing.

## on your computer

### find the name of your sketch's .bin file
Do Sketch -> Export compiled Binary in the Arduino IDE, then Sketch -> Show sketch folder. You will see a file like "ReallyFunSketch.ino.esp32.bin".  

Together with your computer's IP address, You can now build the `url` parameter for the call to `checkFirmware` in the sketch:

`"http://192.168.xxx.xxx:10101/ReallyFunSketch.ino.esp32.bin"`

### load the sketch onto the ESP32
using USB, serial or whatever you normally use.

### start a webserver
in a terminal, cd to your sketch folder and start a webserver. e.g. `python3 -m http.server 10101`  using the same port number you specified in the `url` parameter before.

If everything works you should see some action happening in the terminal window when you reset the ESP. The ESP will download the new firmware, restart and start running.

## how it works

It's based on the [HTTPS\_OTA\_Update example](https://github.com/espressif/arduino-esp32/blob/master/libraries/Update/examples/HTTPS_OTA_Update/HTTPS_OTA_Update.ino) from the ESP32 Arduino libraries.

After connecting to wifi (if configured), the current firmware date is retrieved from ESP flash using the preferences library. Note that this date only reflects the loaded firmware if it was loaded with MassUploader! It will not work if the sketch was uploaded over serial/USB or some other method.  

An HTTP HEAD request is made to the firmware URL with the request header "If-Modified-Since: \<current_firmware_date\>".

If the webserver returns HTTP 304 (NOT_MODIFIED), it means the firmwares are the same or the server version is older. In this case we already have the newest version we can get, and the checkUpload function returns true.

The webserver returns HTTP 200 (OK) if the server has newer firmware. Since our request was just for the HEAD, we don't get the file itself yet, so the URL is passed to the HttpsOTA.begin() function of the HttpsOTAUpdate library, and the firmware download begins. After a successful update, the new firmware date is stored in flash and the ESP reboots. After a reboot, the webserver is checked once again, but this time the firmware dates will be the same and the function will return true.

If there are any HTTP errors or timeouts, or the firmware download fails, the checkUpload function will return false.

## general use

Instead of using cmd+U or ctrl+U to upload new firmware, now you will use ctrl+alt+S or cmd+alt+S to "Export compiled Binary". Once this is complete, press the reset buttons on your ESPs and the new firmware will upload as described above.

If you have several ESPs that are close together, you can connect their reset pins together and press the reset button on one board. This will reset all of them at the same time.  

It helps if your computer has a static IP, reserved IP, or one that doesn't change unless it gets overwritten because of a full DHCP pool (this is usually the case).

## more automaticer mass firmware loading

What if you are too lazy to press the reset button(s), or the ESPs are not easily accessible? 

Look at the AutoReset example. The sketch listens on a UDP port for the string "RESET", and reset the ESP when it gets it.

In the AutoReset sketch folder are a couple of scripts to watch the current directory for changes to .bin files. When one is changed, the script will send out a UDP broadcast 255.255.255.255 (local network only) to reset the ESPs.

When saving the example sketch to a new location, you'll have to copy over the relevant script as well (.sh or .ps1) from the libraries/MassUpload/examples/AutoReset folder.

You should keep the script running while you are developing, and it will pick up the changes in the .bin file that gets generated by the IDE "Export compiled Binary".

WARNING: Because this script sends broadcast, every ESP on your local network running this sketch as-is will be reset! If you have to use this technique for multiple projects at the same time, you can edit the sketch and script to use a different UDP port number.

TODO: make the script send the .bin filename along with "RESET" and the sketch compare the filename to one stored in flash. This way each sketch will only reset the intended ESPs.

The watch.sh script was tested on Mac OS Monterey and Ubuntu 20.10 and requires fswatch and socat to be installed.
The watch.ps1 script was tested on PowerShell 7.3.1 for Mac OS (installed with brew).

## future things

As far as the webserver and automatic reset scripts, I would eventually like to build those into a "tool" for the IDE. Once this is set up with a single sketch it works quite well, but chopping and changing sketches needs you to stop/start the webserver and the reset script if you're using it. It would be nice to just be able to enable this stuff from the IDE. At this time (2023) they are transitioning from the Java IDE 1.x to the Electron IDE 2.x and the new version doesn't have support for user plugins or extensions yet. I don't really want to spend a bunch of time writing the 1.x plugin just for it to be obsoleted!

Optionally use mDNS so we can use computer names instead of IP addresses.

Make separate functions for checking and downloading new firmware?