#ifndef MASSUPLOAD_H
#define MASSUPLOAD_H

#include <Arduino.h>
#include "WiFi.h"
#include <WiFiUdp.h>
#include "HttpsOTAUpdate.h"
#include <Preferences.h>
#include <HTTPClient.h>

// remove all the SPF(...) debug statements if we turned em off
// otherwise, make them printf using the serial port we defined in the main sketch
#ifdef MASSUPLOAD_OUTPUT_SERIAL
#define SPF MASSUPLOAD_OUTPUT_SERIAL.printf
#else
#define SPF(...) do {} while (0)  // this will get optimized out by the compiler
#endif

class MassUpload_ {
  private:
    HttpsOTAStatus_t otastatus;
    size_t firmwareSize = 0;
    size_t firmwareDone = 0;
    void (*progressCallback)(const char* message) = NULL;
    MassUpload_() = default; // Make constructor private

    int connectWifi(const char* ssid, const char* pass);
    int doit(const char* url);
    void downloadProgress (int howmuch);
    static void HttpEvent(HttpEvent_t *event);
    void storeFirmwareDate(String d);
    String getStoredFirmwareDate();
    void updatecb (char* message);


  public:
    static MassUpload_ &getInstance(); // Accessor for singleton instance

    MassUpload_ (const MassUpload_ &) = delete; // no copying
    MassUpload_ &operator=(const MassUpload_ &) = delete;

  public:
    void setCallback(void func(const char* message));
    int checkFirmware(const char* ssid, const char* pass, IPAddress ip, IPAddress mask, const char* url);
    int checkFirmware(const char* ssid, const char* pass, const char* url);
    int checkFirmware(const char* url);
    void stopWifi();
};

extern MassUpload_ &MassUpload;

// sketch can provide a function to receive status updates as we're doing stuff
void MassUpload_::setCallback(void callbackFunc (const char* message)) {
  progressCallback = callbackFunc;
}

// connect to wifi and use static IP
int MassUpload_::checkFirmware(const char* ssid, const char* pass, IPAddress ip, IPAddress mask, const char* url) {
  SPF ("Attempting to connect to SSID %s ", ssid);
  if (ip != IPAddress(0, 0, 0, 0) && mask != IPAddress(0, 0, 0, 0)) {
    SPF ("with static IP %s, netmask %s\n", ip.toString().c_str(), mask.toString().c_str());
    WiFi.config (ip, IPAddress(0, 0, 0, 0), mask, IPAddress(0, 0, 0, 0));
  }
  if (!connectWifi(ssid, pass)) return false;
  
  return doit (url);
}

// conenct to wifi and use DHCP
int MassUpload_::checkFirmware(const char* ssid, const char* pass, const char* url) {
  SPF ("Attempting to connect to SSID %s with DHCP\n", ssid);
  if (!connectWifi(ssid, pass)) return false;
  return doit (url);
}

// if wifi is already handled by the sketch, this version will just do all the HTTP stuff
int MassUpload_::checkFirmware(const char* url) {
  return doit(url);
}

void MassUpload_::stopWifi() {
  SPF ("Stopping WiFi.\n");
  WiFi.mode(WIFI_OFF);
}

int MassUpload_::connectWifi(const char* ssid, const char* pass) {

  // connect to wifi
  updatecb ("Connect WiFi...");

  WiFi.begin(ssid, pass);
  int timeStart = millis();
  while ((WiFi.status() == WL_DISCONNECTED || WiFi.status() == WL_IDLE_STATUS) && millis() - timeStart < 10000) {
    SPF (".");
    delay(500);
  }
  SPF ("\n");

  // if we are not connected, print out some helpful error message and quit
  if (WiFi.status() != WL_CONNECTED) {
    switch (WiFi.status()) {
      case WL_DISCONNECTED:
        SPF ("Timed out! This could mean a bad password or no DHCP server - exiting\n");
      case WL_NO_SSID_AVAIL:
        SPF ("Can't find SSID %s - exiting\n", ssid);
        break;
      default:
        SPF ("WiFi error %i - see https://github.com/esp8266/Arduino/blob/0c897c37a6eab3eab34147219617945a32a9b155/libraries/ESP8266WiFi/src/include/wl_definitions.h#L50 exiting\n", WiFi.status());
        break;
    }
    char wifierr[64];
    sprintf (wifierr, "Wifi error %i", WiFi.status());
    updatecb (wifierr);      // a not-so-helpful message for the callback
    return false;
  }
  
  // if we get here, we connected successfully and DHCP worked, if we configured it
  SPF("My IP = %s\n", WiFi.localIP().toString().c_str());
  return true;
}

// actually do the HTTP checking, downloading and stuff.
int MassUpload_::doit(const char* url) {

  HTTPClient http;
  const char* headerKeys[] = {"Last-Modified", "Content-Length"};
  const size_t numberOfHeaders = 2;
  const char* server_certificate = "suck it";     // we only do http around here

  // send an HTTP HEAD request with the stored firmware date to check if the server version has been modified since
  String storedDate = getStoredFirmwareDate();
  SPF ("Stored firmware: %s\n", storedDate != "" ? storedDate.c_str() : "none");
  http.begin (url);
  http.collectHeaders(headerKeys, numberOfHeaders);
  http.addHeader("If-Modified-Since", storedDate);
  int httpResponseCode = http.sendRequest("HEAD");

  // the only acceptable HTTP responses for us are OK and NOT_MODIFIED. exit if it's anything else
  if (httpResponseCode != HTTP_CODE_OK && httpResponseCode != HTTP_CODE_NOT_MODIFIED) {
    SPF ("HTTP response was %i - exiting\n", httpResponseCode);
    char httperr[64];
    sprintf (httperr, "HTTP code %i", httpResponseCode);
    updatecb (httperr);
    return false;
  }

  // We get HTTP 304 (NOT_MODIFIED) if the server firmware is the same or older, so don't do anything and return
  if (httpResponseCode == HTTP_CODE_NOT_MODIFIED) {
    updatecb ("no update needed!");
    SPF ("No update needed!\n");
    return true;
  }

  // if we get here, we got HTTP 200 (OK) which means the server firmware is newer, so download it
  String serverDate = http.header("Last-Modified");
  firmwareSize = http.header("Content-Length").toInt();
  SPF("Server firmware: %s\n", serverDate.c_str());
  SPF ("New firmware found! Downloading %s\n", url);
  SPF ("Size is %i bytes\n", firmwareSize);

  HttpsOTA.onHttpEvent(HttpEvent);
  HttpsOTA.begin(url, server_certificate);
  while (1) {
    otastatus = HttpsOTA.status();
    if (otastatus == HTTPS_OTA_UPDATING) {
    } else if (otastatus == HTTPS_OTA_SUCCESS) {        // we're done - store date and reboot
      SPF ("\n%i bytes received.\n", firmwareDone);
      SPF ("Storing new firmware date...\n");
      storeFirmwareDate(serverDate);
      SPF ("Rebooting...\n");
      delay(100);
      ESP.restart();
    } else if (otastatus == HTTPS_OTA_FAIL) {
      SPF("\nFirmware Upgrade Fail\n");
      return false;
    }
    delay(500);
  }

  // shouldn't get here (famous last words)
  return false;
}

// print the download progress. If using serial.printf we will print every 10%
// and every 1% we'll call the callback
void MassUpload_::downloadProgress (int data_size) {
  static int lastPercentSPF = -10;   // Hold the last
  static int lastPercentSingle = -1; //
  char m[128];
  firmwareDone += data_size;
  int thisPercent = (float)firmwareDone / firmwareSize * 100;

#ifdef MASSUPLOAD_OUTPUT_SERIAL
  if (thisPercent >= lastPercentSPF + 10) {
    SPF ("%i%%...", thisPercent);
    lastPercentSPF = thisPercent;
  }
#endif

  if (thisPercent > lastPercentSingle) {
    sprintf (m, "Download: %i%%", thisPercent);
    updatecb (m);
    lastPercentSingle = thisPercent;
  }
}

// calllback for http events - use this to update the download progress %
void MassUpload_::HttpEvent(HttpEvent_t *event) {
  switch (event->event_id) {
    case HTTP_EVENT_ERROR:
      break;
    case HTTP_EVENT_ON_CONNECTED:
      break;
    case HTTP_EVENT_ON_DATA:
      MassUpload.downloadProgress(event->data_len);
      break;
    case HTTP_EVENT_ON_FINISH:
      break;
    case HTTP_EVENT_DISCONNECTED:
      break;
  }
}

// store a date in ESP flash
void MassUpload_::storeFirmwareDate(String d) {
  Preferences prefs;
  prefs.begin ("MassUpload", false);
  prefs.putString ("firmwareDate", d);
  prefs.end();
}

// retrieve the date from ESP flash
String MassUpload_::getStoredFirmwareDate() {
  Preferences prefs;
  prefs.begin ("MassUpload", false);
  if (prefs.isKey("firmwareDate")) {
    String storedDate = prefs.getString("firmwareDate");
    prefs.end();
    return storedDate;
  } else {
    prefs.end();
    return "";
  }
}

// call the user-configured callback, if it was set
void MassUpload_::updatecb (char* message) {
  if (progressCallback != NULL) {
    progressCallback ((const char*) message);
  }
}

MassUpload_ &MassUpload_::getInstance() {
  static MassUpload_ instance;
  return instance;
}

MassUpload_ &MassUpload = MassUpload.getInstance();

#endif